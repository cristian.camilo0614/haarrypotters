import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import {CommonModule} from '@angular/common';


import { HouseDetailComponent } from './components/house-detail/house-detail.component';
import { BuscarComponent } from './pages/buscar/buscar.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'house/:id',
    component: HouseDetailComponent,
  },
  {
    path: 'search/:texto',
    component: BuscarComponent,
  },

  {
    path: '**',
    redirectTo: '/home',
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
