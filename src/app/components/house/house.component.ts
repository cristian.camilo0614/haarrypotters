import { Component, OnInit, Input } from '@angular/core';
import { House } from '../../interfaces/modeloInterface';
import { InformacionService } from '../../services/informacion.service';

import { Router } from '@angular/router';

/**
 * Esta clase se encarga de la logica de negocion en la cual consumimos el servicio
 * para que nos retorne las diferentes casas
 * @author Cristian Camilo Diaz <cristian.camilo0614@gmail.com>
 * @version 2020-11-04
 */

@Component({
  selector: 'app-house',
  templateUrl: './house.component.html',
  styleUrls: ['./house.component.scss'],
})

/**
 * declaramos diferentes variables, y el input el cual compartira informacion
 * a los diferentes componentes en este caso object de tipo house
 */
export class HouseComponent implements OnInit {
  @Input() house: House[];
  respuesta: any;
  respu: any;
  zise: any;

  constructor(
    private router: Router,
    private infoService: InformacionService
  ) {}

  ngOnInit(): void {
    this.getHouses();
    
  }

  /**
   * Consumimos el servicio el cual nos retorna las informacion de las casas
   */
  getHouses() {
    this.infoService.getHouses().subscribe((resp: any) => {
      console.log(resp);
      this.respuesta = resp;
      this.house = this.respuesta.resp;
    });
  }

  /**
   * Método encargado de obtenernos el id de la casa en el cual se hace click
   * para que con el _id podamos ver los detalles de la casa seleccionada
   */
  seeHouse(house: House) {
    this.router.navigate(['/house', house._id]);
  }

  
}
