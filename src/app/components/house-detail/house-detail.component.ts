import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InformacionService } from '../../services/informacion.service';
import { House } from '../../interfaces/modeloInterface';

/**
 * Esta clase se encarga de la logica para imprimir los detalles de las casas
 * @author Cristian Camilo Diaz <cristian.camilo0614@gmail.com>
 * @version 2020-11-04
 */

@Component({
  selector: 'app-house-detail',
  templateUrl: './house-detail.component.html',
  styleUrls: ['./house-detail.component.scss'],
})
export class HouseDetailComponent implements OnInit {
  public respuesta: House;
  public house: House;

  /**
   *  Construimos el servicio el cual tendra la informacion de los detalles de la casa
   *  y tambie utilizaremos el activateRouter para traernos el id de la casa para ver sus detalles
   *  utilizando el snapshot.params; los colocamos en el ngOInit para que cuando renderice este componente
   * inmediantamente obtengamos la data.
   *
   * @param activatedRouter
   * @param inforService
   */

  zise: any;

  constructor(
    private activatedRouter: ActivatedRoute,
    private inforService: InformacionService
  ) {}

  ngOnInit(): void {
    const { id } = this.activatedRouter.snapshot.params;
    this.inforService.getHouse(id).subscribe((house) => {
      this.respuesta = house;
    });
  }
}
