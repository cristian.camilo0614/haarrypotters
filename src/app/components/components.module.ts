import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { HouseComponent } from './house/house.component';
import { HouseDetailComponent } from './house-detail/house-detail.component';



@NgModule({
  declarations: [
    NavbarComponent,    
    HouseComponent,
    HouseDetailComponent
  ],
  exports: [
    NavbarComponent,    
    HouseComponent,
    HouseDetailComponent

  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class ComponentsModule { }
