import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

/**
 * Esta clase se encarga de obtener el texto que digitamos el la caja del buscador
 * @author Cristian Camilo Diaz <cristian.camilo0614@gmail.com>
 * @version 2020-11-04
 */


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {

  
  constructor(private route: Router) {}

  ngOnInit(): void {}

  /**
   * este metodo es el encargado de obtener el texto que digitamos para la busqueda
   * de tipo string, el cual valida que no este vacio, y nos redireccion aa pagina la cual
   * renderizara el personaje a buscar
   * @param texto 
   */
  searchCharacter(texto: string) {
    texto = texto.trim();
    if (texto.length === 0) {
      return;
    }
    this.route.navigate(['/search', texto]);
    
  }
}
