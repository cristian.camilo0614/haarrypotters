import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { House, Character } from '../interfaces/modeloInterface';

/**
 * Esta es la clase servicio la cual hace las peticiones http para consumir la 
 * api  de harry poter con sus respectivos endPoint, 
 * @author Cristian Camilo Diaz <cristian.camilo0614@gmail.com>
 * @version 2020-11-04
 */


@Injectable({
  providedIn: 'root'
})
export class InformacionService {

  House: House[];

  constructor(private http: HttpClient ) { }

  /**
   * Metodo encargado de retornar las diferentes casas, en nuestra carpeta de environments 
    *  tanto para "environments.ts" como  "environments.product.ts"
    * colocamos la Base de nuestra URL y la KEY que nos genera la documentacion de la api 
    *  Necesarias para consumir los datos
   */
  getHouses():Observable<House> {
    return this.http.get<House>(
     `${environment.baseUrlAPI}houses?key=${environment.API_KEY}`
    );
  }


  /**
   * Este metodo nos retorna con el id, la casa seleccionada para asi poder ver sus detalles
   * el cual le entra el parametro id el cuel es captura al presionar el boton de ver detalles casa
   */

  getHouse(id:string){
   // return this.House[idx];
   return this.http.get<House>(`${environment.baseUrlAPI}houses/${id}?key=${environment.API_KEY}`
   );
  }

  
  /**
   * Este metodo nos trae del Json todos los personajes
   */

  getCharacters(termino:string):Observable<Character> {
    return this.http.get<Character>(
     `${environment.baseUrlAPI}characters?key=${environment.API_KEY}&name=${termino}`
    );
  }


}