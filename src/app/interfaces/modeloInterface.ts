export interface Character {
  id: number;
  name: string;
  role: string;
  house: string;
  school: string;
  bloodStatus: string;
  species: string;
}

export interface House {
  _id: number;
  name: string;
  mascot: string;
  school: string;
  members: []
 
}
