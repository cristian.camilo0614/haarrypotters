import { Component, OnInit } from '@angular/core';
import { InformacionService } from '../../services/informacion.service';
import { House } from '../../interfaces/modeloInterface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public house: House[] = [];
  respuesta: any;

  constructor(private infoService: InformacionService) {}

  ngOnInit(): void {
    this.infoService.getHouses().subscribe((resp: any) => {
      this.respuesta = resp;
      this.house = this.respuesta.resp;
    });
  }
}
