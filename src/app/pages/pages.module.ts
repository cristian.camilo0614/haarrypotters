import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './home/home.component';

import { BuscarComponent } from './buscar/buscar.component';
import { ComponentsModule } from '../components/components.module';



@NgModule({
  declarations: [
    HomeComponent, 
    
    BuscarComponent
  ],

  imports: [
    CommonModule,
    ComponentsModule
  ],
  exports: [
    HomeComponent, 
    
    BuscarComponent
  ]
})
export class PagesModule { }
