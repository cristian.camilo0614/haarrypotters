import { Component, OnInit,Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InformacionService } from '../../services/informacion.service';
import {  Character } from '../../interfaces/modeloInterface';

/**
 * Esta clase se encarga de comparar el nombre que escribimos en el buscador
 * con los miembros que existen en dicha casa retornando el personaje
 * el cual coicida el nombre y apellido EXACTAMENTE
 * @author Cristian Camilo Diaz <cristian.camilo0614@gmail.com>
 * @version 2020-11-04
 */



@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.scss'],
})
export class BuscarComponent implements OnInit {

  @Input() house: any={};

  public texto: string = '';  
  public character: Character;

  constructor(private activatedRoute: ActivatedRoute,
    private informacionService: InformacionService) {}

    ngOnInit(): void {

      this.activatedRoute.params.subscribe( params => {
        
        this.texto = params.texto;
  
        this.informacionService.getCharacters( params.texto ).subscribe( character => {
          console.log('OBJETO ENCONTRADO',character);
          this.character = character;
        })
      })

      
  
    }
  
  }