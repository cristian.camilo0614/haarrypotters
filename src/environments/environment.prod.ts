export const environment = {
  production: true,
  baseUrlAPI: "https://www.potterapi.com/v1/",
  API_KEY:"$2a$10$dc.tqg2FLjgg/1U0fmuOjOjanB9dZHt7N7uoGFhUE8lzKElKvuy0S",
};
